import pm2 from "pm2";
import { parse_csv } from "./helpers/csv.js";
import fs from "fs";

var json = JSON.parse(fs.readFileSync('./processes.json', 'utf8'));
const INSTANCE_NUMBER = json.apps[0].instances;

// parse csv to json
const len = await parse_csv('./helpers/etablissement.csv');

pm2.connect(function (err) {
    if (err) {
        console.error(err)
        process.exit(2)
    }

    // create workers
    const workers = [];
    for (let i = 0; i < INSTANCE_NUMBER; i++) {
        let worker = {
            env: {
                pid: i,
                files: []
            }
        }
        
        // affect files to worker
        for (let j = 0; j < len; j++) {
            if (j%INSTANCE_NUMBER == i) {
                worker.env.files.push(`./outputs/output-${j}.csv`)
            }
        }

        workers.push(worker);
    }

    // for each worker start process
    for (const index in workers) {
        pm2.start({
            script: 'worker.js',
            name: 'worker',
            env: workers[index].env
        }, function (err, apps) {
            if (err) {
                console.error(err)
                return pm2.disconnect()
            }

            pm2.list((err, list) => {
                console.log(err, list)

                pm2.restart('api', (err, proc) => {
                    // Disconnects from PM2
                    pm2.disconnect()
                })
            })
        })
    }
})

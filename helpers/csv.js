import fs from 'fs';
import { split } from 'csv-split-stream';

export async function parse_csv(path) {
    let len = 0;
    const _ = await split(
        fs.createReadStream(path),
        {
            lineLimit: 1000
        },
        (index) => {
            if (index < 20) {
                len += 1;
                return fs.createWriteStream(`./outputs/output-${index}.csv`);
            }
            else {
                return fs.createWriteStream('./outputs/poubelle.csv');
            }
        }
    )
        .then(csvSplitResponse => {
            console.log('csvSplitStream succeeded.', csvSplitResponse);
            // outputs: {
            //  "totalChunks": 350,
            //  "options": {
            //    "delimiter": "\n",
            //    "lineLimit": "10000"
            //  }
            // }
        }).catch(csvSplitError => {
            console.log('csvSplitStream failed!', csvSplitError);
        });
    
    return len;
}

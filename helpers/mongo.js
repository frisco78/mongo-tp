import { model, Schema } from 'mongoose';

const EtablissementSchema = new Schema({
	nic: { type: Number, required: false },
	siren: { type: Number, required: false, unique: true },
	siret: { type: Number, required: true, unique: true },
	statutDiffusionEtablissement: { type: String, required: false },
	dateCreationEtablissement: { type: Date, required: false },
	activitePrincipaleRegistreMetiersEtablissement: { type: String, required: false },
	dateDernierTraitementEtablissement: { type: String, required: false },
	etablissementSiege: { type: Boolean, required: false },
	nombrePeriodesEtablissement: { type: Number, required: false },
	libelleVoieEtablissement: { type: String, required: false },
	codePostalEtablissement: { type: String, required: false },
	libelleCommuneEtablissement: { type: String, required: false },
	codeCommuneEtablissement: { type: String, required: false },
	dateDebut: { type: Date, required: false },
	etatAdministratifEtablissement: { type: String, required: false },
	activitePrincipaleEtablissement: { type: String, required: false },
	nomenclatureActivitePrincipaleEtablissement: { type: String, required: false },
	caractereEmployeurEtablissement: { type: String, required: false },
});

const Etablissement = model('Etablissement', EtablissementSchema);

export { Etablissement };

import mongoose from "mongoose";
import fs from "fs";
import { Etablissement } from "./helpers/mongo.js";
import csvToJson from "convert-csv-to-json";

const files_string = process.env.files;

const files = JSON.parse(files_string);

// connect o bdd
await mongoose.connect('mongodb://127.0.0.1/tp');

for (const index in files) {
    // transform csv to json
    let json = [];
    try {
        json = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv(files[index]);
    }
    // if file dont exist, skip
    catch (e) {
        console.log(e.message)
        continue;
    }
    // insert in bdd
    await Etablissement.insertMany(json, { ordered: false });
    // delete file
    fs.unlink(files[index], () => console.log(`File ${files[index]} as been deleted`));
}
